let product = [
  {
    nama: 'Indomie',
    harga: 3000,
    rating: 5,
    likes: 150,
  },
  {
    nama: 'Laptop',
    harga: 4000000,
    rating: 4.5,
    likes: 123,
  },
  {
    nama: 'Aqua',
    harga: 3000,
    rating: 4,
    likes: 250,
  },
  {
    nama: 'Smart TV',
    harga: 4000000,
    rating: 4.5,
    likes: 42,
  },
  {
    nama: 'Headphone',
    harga: 4000000,
    rating: 3.5,
    likes: 90,
  },
  {
    nama: 'Very Smart TV',
    harga: 4000000,
    rating: 3.5,
    likes: 87,
  },
];

let harga = product.sort((a, b) => {
  return a.harga - b.harga;
});

let urutkanData = product.sort((a, b) => {
  if (a.harga === a.harga || a.rating === a.rating) {
    return a.harga - b.harga && a.rating - b.rating;
  }
});
console.log(urutkanData);
